Installing Python + tensorflow

There are 2 main ways to install Python. I�ll proceed with installing Python using the Anaconda distribution, as it comes with some useful packages already installed, which reduces the chance for things going wrong during installation. It also allows for creating virtual environments, which is useful here, as ideally we want 2 environments for the CPU and GPU versions of tensorflow.
1. Download the Anaconda installer from https://www.anaconda.com/download/ for the Python 3.6 64-bit version.
2. Install Anaconda
3. Using the Anaconda Prompt create a conda environment called tensorflow:

C:> conda create -n tensorflow pip python=3.5 

4. Activate the conda environment by writing:

C:> activate tensorflow

5. For CPU-only tensorflow Install tensorflow version 1.5:

(tensorflow)C:> pip install tensorflow==1.5

The CPU version is enough for all the program function, albeit training is much slower. If that is  enough, proceed with installing OpenCV and Kivy.
6. For GPU version of tensorflow first verify that you have a Nvidia GPU and that it supports  CUDA Compute Capability 3.0 or higher. See https://developer.nvidia.com/cuda-gpus or verify it otherwise.
Its important that the  exact given versions of the CUDA Toolkit and cuDNN is installed.
7. Install CUDA� Toolkit 9.0. Consult https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/. Ensure that you append the relevant Cuda pathnames to the %PATH% environment variable as described in the NVIDIA documentation.
8. Install cuDNN v7.0. Consult https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html#install-windows. To install this software you need to join the NVIDIA Developer Program.
9. Create an environment for the GPU version of tensorflow:

C:> conda create -n tensorflowGPU pip python=3.5 

10. Install tensorflow-gpu version 1.5

(tensorflow)C:> pip install tensorflowGPU==1.5

Instructions based on: https://www.tensorflow.org/install/ 


Installing OpenCV
Type this command in the environment of your choice (here �tensorflow�)

(tensorflow)C:> pip install opencv-contrib-python

Installing Kivy
Type those commands in the python environment of your choice

	pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew
	pip install kivy
