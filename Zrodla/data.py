import glob
import os
import json
import random
from collections import namedtuple
from itertools import permutations, combinations

import numpy as np
import tensorflow as tf
import cv2

from show_image import show

ISIZE = 32
CHANNELS = 1

def get_combination(labels):
    Card = namedtuple('Card', ['color', 'rank'])

    cards = [Card(label // 13, label % 13) for label in labels]        
    permutations_of_5 = permutations(cards, r=5)
    combinations_of_4 = combinations(cards, r=4)
    combinations_of_3 = combinations(cards, r=3)
    combinations_of_2 = combinations(cards, r=2)

    flush = all(card.color == cards[0].color for card in cards)
    straight = any(
        all(p[i+1].rank == p[i].rank + 1 for i in range(4))
        for p in permutations_of_5        
    )
    four_of_a_kind = [
        c for c in combinations_of_4
        if all(card.rank == c[0].rank for card in c)        
    ]
    three_of_a_kind = [
        c for c in combinations_of_3
        if all(card.rank == c[0].rank for card in c)        
    ]

    pairs = [
        c for c in combinations_of_2
        if c[0].rank == c[1].rank        
    ]
    # 3s cant be part of 4s, 2s cant be part of 3s
    if four_of_a_kind:
        three_of_a_kind = []
        pairs = []
    if three_of_a_kind:
        pairs = [p for p in pairs if p[0].rank != three_of_a_kind[0][0].rank]    
            
    if straight and flush:
        high_card = max(card.rank for card in cards)
        if high_card == 12: # Ace 
            return "Royal flush"

        high_card_desc = get_rank(high_card)
        return "Straight flush, {} high".format(high_card_desc)
    
    if four_of_a_kind:
        high_card = max(card.rank for card in four_of_a_kind[0])
        high_card_desc = get_rank(high_card)
        return "Four of a kind, {} high".format(high_card_desc)

    if three_of_a_kind and pairs:
        high_card = max(card.rank for card in three_of_a_kind[0])
        high_card_desc = get_rank(high_card)
        return "Full house, {} high".format(high_card_desc)

    if flush:
        high_card = max(card.rank for card in cards)
        high_card_desc = get_rank(high_card)
        return "Flush, {} high".format(high_card_desc)

    if straight:
        high_card = max(card.rank for card in cards)
        high_card_desc = get_rank(high_card)
        return "Straight, {} high".format(high_card_desc)

    if three_of_a_kind:
        high_card = max(card.rank for card in three_of_a_kind[0])
        high_card_desc = get_rank(high_card)
        return "Three of a kind, {} high".format(high_card_desc)

    if len(pairs) == 2:
        first = pairs[0][0].rank
        second = pairs[1][0].rank
        if second > first:
            first, second = second, first
        
        first = get_rank(first)
        second = get_rank(second)

        return "Two pairs: {}s and {}s".format(first, second)

    if len(pairs) == 1:
        rank = get_rank(pairs[0][0].rank)

        return "Pair of {}s".format(rank)

    high_card = max(card.rank for card in cards)
    high_card_desc = get_rank(high_card)

    return "High card {}".format(high_card_desc)

def show_data(data, labels):
    for image, label in zip(data[:10], labels[:10]):
        print(get_description(label))
        show(np.reshape(image, [ISIZE, ISIZE, CHANNELS]))

def load_image(path):                
    image = cv2.imread(path)
    return image   

def load_images(images_folder, limit=None):
    paths = glob.glob(os.path.join(images_folder, "*.png"))
    paths = sorted(paths, key=lambda x: int(os.path.basename(x).split('.')[0].split('-')[0]))
    images = []
    for i, p in enumerate(paths):
        if limit and i == limit:
            break;
        images.append(load_image(p))

    return images

def load_labels(filename):
    with open(filename, 'r') as fp:
        loaded = json.load(fp)
        sorted_labels = sorted(loaded, key=lambda x: x[0])        
        return sorted_labels

def take_out_bad_data(raw_images, raw_labels):
    cleaned_labels = [l for l in raw_labels if l[1] != 'not_card']
    cleaned_images = [im for i, im in enumerate(raw_images) if raw_labels[i][1] != 'not_card']
    
    return (cleaned_images, cleaned_labels)

def split_data(images, labels, split_ratio):
    train_set_size = int(len(images) * split_ratio)

    labels = np.reshape(labels, [-1,1])
    labeled_data = np.concatenate((images, labels), axis=1)
    train, test = tf.split(tf.random_shuffle(labeled_data, seed=1234), [train_set_size, -1])

    sess = tf.Session()
    train = train.eval(session=sess)
    test = test.eval(session=sess)

    train_data = train[..., 0:ISIZE*ISIZE*CHANNELS]
    train_labels = train[..., ISIZE*ISIZE*CHANNELS]

    eval_data = test[..., 0:ISIZE*ISIZE*CHANNELS]
    eval_labels = test[..., ISIZE*ISIZE*CHANNELS]

    return (train_data, train_labels, eval_data, eval_labels)

def get_label(card_color, card_rank):
    color_label = {
        'kier': 0,
        'karo': 1,
        'pik': 2,
        'trefl': 3,
    }[card_color]

    rank_label = {
        '2': 0,
        '3': 1,
        '4': 2,
        '5': 3,
        '6': 4,
        '7': 5,
        '8': 6,
        '9': 7,
        '10': 8,
        'B': 9,
        'D': 10,
        'K': 11,
        'A': 12,
    }[card_rank]    

    return color_label * 13 + rank_label

def get_rank(rank_label):
    return [
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        'B',
        'D',
        'K',
        'A',
    ][rank_label]    

def get_color(color_label):
    return [
        'kier',
        'karo',
        'pik',
        'trefl',
    ][color_label]


def get_classes(label):
    try:
        return get_color(label // 13), get_rank(label % 13)        
    except IndexError:
        return None, None

def get_description(label):
    color, rank = get_classes(label)
    if color == None:
        return 'unknown'    
    return "{}, {}".format(color, rank)
    
def balance(data, labels):
    data_split = [[] for i in range(52)]
    
    for i, label in enumerate(labels):
        data_split[label].append(i)

    biggest_size = max(len(class_) for class_ in data_split)

    to_duplicate = []
    for i in range(52):
        difference = biggest_size - len(data_split[i])
        for _ in range(difference):
            to_duplicate.append(random.choice(data_split[i]))

    copied_data = np.array([data[i] for i in to_duplicate])
    copied_labels = np.array([labels[i] for i in to_duplicate])

    return np.concatenate((data, copied_data)), np.concatenate((labels, copied_labels))

    
def for_training_and_eval(images_folder, labels_filename, split_ratio=0.9, num_corners=4):
    raw_labels = load_labels(labels_filename)
    raw_data = load_images(images_folder)
    
    # Repeat the labels num_corner times, ex. [1,2,3] -> [1,1,1,1,2,2,2,2,3,3,3,3]
    labels = sum(zip(*((raw_labels,) * num_corners)), ())

    processed_data = []
    for image in raw_data:
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image = cv2.resize(image, (ISIZE, ISIZE))
        processed_data.append(image)
    
    if len(labels) != len(processed_data):
        print("Labels and data not the same size: D{}, L{}".format(len(processed_data), len(labels)))
        return
        
    processed_data, labels = take_out_bad_data(processed_data, labels)

    labels = np.array([get_label(l[1], l[2]) for l in labels], dtype=np.uint8)
    data = np.array(processed_data, dtype=np.uint8)
    data = np.reshape(data, [-1, ISIZE * ISIZE * CHANNELS])

    train_data, train_labels, eval_data, eval_labels = split_data(data, labels, split_ratio)

    train_data, train_labels = prepare_data(train_data, train_labels)
    eval_data, eval_labels = prepare_data(eval_data, eval_labels)

    #show_data(train_data, train_labels)
    return {'train': (train_data, train_labels), 'eval': (eval_data, eval_labels)}

def for_labeling(images_folder):
    images = load_images(images_folder)
    images = np.array(images, dtype=np.uint8)
    images = np.reshape(images, [-1, ISIZE * ISIZE * CHANNELS])
    images = np.array(images, dtype=np.float32)
    images /= 255
    
    return images

def sample(images_folder):
    sample = load_images(images_folder, limit=50)
    sample = sorted(sample, key=lambda x: random.random())[:10]
    sample = np.array(sample, dtype=np.uint8)
    sample = np.reshape(sample, [-1, ISIZE * ISIZE * CHANNELS])
    sample = np.array(sample, dtype=np.float32)
    sample /= 255

    return sample

def prepare_images(images):
    prepared = []
    for card in images:
        for im in card:
            im = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
            im = cv2.resize(im, (ISIZE, ISIZE))
            prepared.append(im)

    prepared = np.array(prepared, dtype=np.uint8)
    prepared = np.reshape(prepared, [-1, ISIZE * ISIZE * CHANNELS])
    prepared = np.array(prepared, dtype=np.float32)
    prepared /= 255

    return prepared

def prepare_data(images, labels):
    images = np.array(images, dtype=np.float32)
    images /= 255
    labels = np.array(labels, dtype=np.int32)

    return images, labels 


    