import cv2

def show(image, scaling=1):
    to_show = cv2.resize(image, (0, 0), fx=scaling, fy=scaling)
    cv2.imshow("title", to_show)
    cv2.moveWindow("title", 0, 0)
    cv2.waitKey(0) 
    cv2.destroyAllWindows()