import data
from show_image import show

import numpy as np
import tensorflow as tf
import cv2

import glob
import os
import json
import logging
from collections import Counter

ISIZE = 32
CHANNELS = 1
NUM_CLASSES = 13 * 4
SAVE_PATH = "./model"

tf.logging.set_verbosity(tf.logging.INFO)

biai_classifier = None

class EarlyStoppingHook(tf.train.SessionRunHook):
    """Hook that requests stop at a specified step."""

    def __init__(self, monitor='val_loss', min_delta=0, patience=0,
                 mode='auto'):
        """
        """
        self.monitor = monitor
        self.patience = patience
        self.min_delta = min_delta
        self.wait = 0
        if mode not in ['auto', 'min', 'max']:
            logging.warning('EarlyStopping mode %s is unknown, '
                            'fallback to auto mode.', mode, RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
        elif mode == 'max':
            self.monitor_op = np.greater
        else:
            if 'acc' in self.monitor:
                self.monitor_op = np.greater
            else:
                self.monitor_op = np.less

        if self.monitor_op == np.greater:
            self.min_delta *= 1
        else:
            self.min_delta *= -1

        self.best = np.Inf if self.monitor_op == np.less else -np.Inf

    def begin(self):
        # Convert names to tensors if given
        graph = tf.get_default_graph()
        self.monitor = graph.as_graph_element(self.monitor)
        if isinstance(self.monitor, tf.Operation):
            self.monitor = self.monitor.outputs[0]

    def before_run(self, run_context):  # pylint: disable=unused-argument
        return tf.train.SessionRunArgs(self.monitor)

    def after_run(self, run_context, run_values):
        current = run_values.results

        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
        else:
            self.wait += 1
            if self.wait >= self.patience:
                run_context.request_stop()

def cnn_model_fn(features, labels, mode):
    """Model function for CNN"""
    # input layer
    input_layer = tf.reshape(features["x"], [-1, ISIZE, ISIZE, 1])

    # convolutional layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=32,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu
    )

    # pooling layer #1
    pool1 = tf.layers.max_pooling2d(
        name='pooling1',
        inputs=conv1,
        pool_size=[2, 2],
        strides=2 
    )

    # convolutional layer #2 and pooling layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=128,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu
    )
    pool2 = tf.layers.max_pooling2d(
        name='pooling2',
        inputs=conv2,
        pool_size=[2, 2],
        strides=2
    )

    # dense layer
    pool2_flat = tf.reshape(pool2, [-1, ISIZE//4 * ISIZE//4 * CHANNELS * 128])
    dense = tf.layers.dense(
        inputs=pool2_flat,
        units=1024,
        activation=tf.nn.relu,
        #kernel_regularizer=tf.contrib.layers.l2_regularizer(0.01),
    )
    dropout = tf.layers.dropout(
        inputs=dense, rate=0.7, training=(mode == tf.estimator.ModeKeys.TRAIN)
    )

    # logits layer
    logits = tf.layers.dense(inputs=dropout, units=NUM_CLASSES)

    predictions = {
        "classes": tf.argmax(input=logits, axis=1),
        "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate loss
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    #regularization = tf.losses.get_regularization_loss()
    #loss += regularization

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(
            learning_rate=0.001,
            beta1=0.9,
            beta2=0.999,
            epsilon = 0.1,
        )
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step()
        )
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluations metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"]
        )
    }
    return tf.estimator.EstimatorSpec(
        mode=mode,
        loss=loss,
        eval_metric_ops=eval_metric_ops
    )

def evaluate(data_folder=None, labels_file=None, num_corners=4, dataset=None):
    global biai_classifier

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

    with session:
        if dataset is None:
            print(num_corners)
            dataset = data.for_training_and_eval(data_folder, labels_file, num_corners=num_corners)

        if biai_classifier is None:
            biai_classifier = tf.estimator.Estimator(
                model_fn=cnn_model_fn, model_dir=SAVE_PATH
            )

        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': dataset['eval'][0]},
            y=dataset['eval'][1],
            num_epochs=1,
            shuffle=False
        )
        eval_results = biai_classifier.evaluate(
            input_fn=eval_input_fn,
        )        
        
        train_set_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': dataset['train'][0]},
            y=dataset['train'][1],
            num_epochs=1,
            shuffle=False
        )
        train_set_results = biai_classifier.evaluate(
            input_fn=train_set_input_fn,
        )
        
        print("TRAIN SET RESULTS:")
        print(train_set_results)
        print("EVAL SET RESULTS:")
        print(eval_results)


def show_wrong_predictions(data_folder, labels_file):
    global biai_classifier

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

    with session:
        dataset = data.for_training_and_eval(data_folder, labels_file, num_corners=2)

        if biai_classifier is None:
            biai_classifier = tf.estimator.Estimator(
                model_fn=cnn_model_fn, model_dir=SAVE_PATH
            )    

        print("TRAIN SET:")
        input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': dataset['train'][0]},
            shuffle=False
        )

        predictions = biai_classifier.predict(
            input_fn,
        )
        
        comparisons = list(zip(dataset['train'][0], dataset['train'][1], predictions))
        incorrect = [x for x in comparisons if x[1] != x[2]['classes']]
        print("{} incorrect of {}".format(len(incorrect), len(dataset['train'][0])))        

        print("Wrong predictions:\n")
        for image, label, prediction in incorrect:
            true_desc = data.get_description(label)
            prediction_desc = data.get_description(prediction['classes'])
            print("True: {}, Predicted: {}".format(true_desc, prediction_desc))
            image = np.reshape(image, [ISIZE, ISIZE])
            show(image, 5)


        print("EVAL SET:")
        input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': dataset['eval'][0]},
            shuffle=False
        )

        predictions = biai_classifier.predict(
            input_fn,
        )
        
        comparisons = list(zip(dataset['eval'][0], dataset['eval'][1], predictions))
        incorrect = [x for x in comparisons if x[1] != x[2]['classes']]
        print("{} incorrect of {}".format(len(incorrect), len(dataset['eval'][0])))        

        print("Wrong predictions:\n")
        for image, label, prediction in incorrect:
            true_desc = data.get_description(label)
            prediction_desc = data.get_description(prediction['classes'])
            print("True: {}, Predicted: {}".format(true_desc, prediction_desc))
            image = np.reshape(image, [ISIZE, ISIZE])
            show(image, 5)

def visualize():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

    with session:
        sample = data.sample("training_data")
        
        biai_classifier = tf.estimator.Estimator(
            model_fn=cnn_model_fn, model_dir=SAVE_PATH
        )

        visualization_hook = VisualizationHook()
        #tensors_to_log = {"probabilities": "softmax_tensor"}
        #logging_hook = tf.train.LoggingTensorHook(
        #    tensors=tensors_to_log, at_end=True
        #)

        input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': sample},
            shuffle=False
        )
        predictions = biai_classifier.predict(
            input_fn,
            hooks=[visualization_hook]
        )     
        
    
def train_and_eval(data_folder, labels_file, num_corners=4):
    global biai_classifier

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

    with session:
        dataset = data.for_training_and_eval(data_folder, labels_file, num_corners=num_corners)
        train_data, train_labels = dataset['train']
        train_data, train_labels = data.balance(train_data, train_labels)

        if biai_classifier is None:
            biai_classifier = tf.estimator.Estimator(
                model_fn=cnn_model_fn, model_dir=SAVE_PATH
            )

        tensors_to_log = {"probabilities": "softmax_tensor"}
        logging_hook = tf.train.LoggingTensorHook(
            tensors=tensors_to_log, every_n_iter=500
        )

        early_stopping_hook = EarlyStoppingHook(
            monitor='sparse_softmax_cross_entropy_loss/value',
            patience=5000
        )


        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': train_data},
            y=train_labels,
            batch_size=128,
            num_epochs=None,
            shuffle=True
        )
        biai_classifier.train(
            input_fn=train_input_fn,
            steps=200000,
            hooks=[early_stopping_hook]
        )
        evaluate(dataset=dataset)

# accepts predictions grouped by num_corners(for each card)
def prediction_voting(predictions, num_corners=4):
    predictions = list(predictions)
    decisions = []
    for i in range(0, len(predictions), num_corners):
        group = predictions[i:i+num_corners]
        counter = Counter()
        for prediction in group:
            counter[prediction['classes']] += 1            
        most_common = counter.most_common(2)
        winner_class = most_common[0][0]
        winner_votes = most_common[0][1]
        
        # If there is a tie
        if len(most_common) > 1 and most_common[0][1] == most_common[1][1]:            
            best_prediction = max(group, key=lambda x: max(x['probabilities']))
            winner_class = best_prediction['classes']

        winner_prob = max([max(p['probabilities']) for p in group if p['classes'] == winner_class])
                
        decisions.append({'classes': winner_class, 'votes': winner_votes,
         'predictions': group})

    return decisions

def predict(input_images, num_corners=4):
    global biai_classifier
    with tf.device('/cpu:0'):   
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        session = tf.Session(config=config)

        with session:
            input_data = data.prepare_images(input_images)

            if biai_classifier is None:
                biai_classifier = tf.estimator.Estimator(
                    model_fn=cnn_model_fn, model_dir=SAVE_PATH
                )
        
            input_fn = tf.estimator.inputs.numpy_input_fn(
                x={'x': input_data},
                shuffle=False
            )

            predictions = biai_classifier.predict(
                input_fn,
            )     
            
    return prediction_voting(predictions, num_corners)

if __name__ == "__main__": 
    import sys

    if sys.argv[1].startswith("train"):
        train_and_eval("training_data", "generated_labels.txt", num_corners=2)
    elif sys.argv[1].startswith("eval"):
        evaluate("training_data", "generated_labels.txt", num_corners=2)
    elif sys.argv[1].startswith('vis'):
        visualize()
    elif sys.argv[1].startswith('show'):
        show_wrong_predictions("training_data", "generated_labels.txt")