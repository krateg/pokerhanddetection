import sys
import os
from glob import glob

import data
from collections import Counter

def print_num_classes():
    dataset = data.for_training_and_eval("training_data", "generated_labels.txt")
    print("Dataset loaded")
    train_data, train_labels = dataset['train']    
    train_data, train_labels = data.balance(train_data, train_labels)
    eval_labels = dataset['eval'][1]

    counter_train = Counter()
    counter_eval = Counter()
    for label in train_labels:
        counter_train[data.get_description(label)] += 1
    for label in eval_labels:
        counter_eval[data.get_description(label)] += 1

    print("Train balance: ")
    print(counter_train)

    print("Eval balance: ")
    print(counter_eval)

if __name__ == "__main__":
    #if len(sys.argv) == 2:
    #    if sys.argv[1] == 'classes':
    print_num_classes()




