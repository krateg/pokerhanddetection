import data
import classifier
from show_image import show

import cv2

import os
import sys
from glob import glob
import json

OUTPUT_FOLDER = 'training_data_split'

def split():
    try:    
        os.mkdir(OUTPUT_FOLDER)
    except FileExistsError:
        print("Folder: '{}' already exists".format(OUTPUT_FOLDER))

    try:    
        for i in range(52):
            os.mkdir(OUTPUT_FOLDER + '\\' + str(i))
    except FileExistsError:
        print("Folder: '{}' already exists".format(i))
        
    

    images = data.load_images('training_data')
    images = [images[i:i+4] for i in range(0, len(images), 4)]
    predictions = classifier.predict(images)
    
    for n_pred, (prediction, group) in enumerate(zip(predictions, images)):
        label = prediction['classes']        
        for i, image in enumerate(group):
            path = '{}/{}/{}-{}.png'.format(OUTPUT_FOLDER, label, n_pred, i)
            cv2.imwrite(path, image)

def create_labels(input_folder, labels_filename,
        num_corners=4, check_completeness=True, raw_folder=None):
    labels = []
    for i in range(52):
        paths = glob("{}\\{}\\*.png".format(input_folder, i))
        paths = sorted(paths, key=lambda x: int(os.path.basename(x).split('-')[0]))
        for path in paths[::num_corners]:
            color, rank = data.get_classes(i)
            index = int(os.path.basename(path).split('-')[0])
            labels.append([index, color, rank])
    
    if check_completeness and raw_folder:
        paths = glob("{}\\*.png".format(raw_folder))
        paths = sorted(paths, key=lambda x: int(os.path.basename(x).split('-')[0]))    
        
        labels_indexes = [x[0] for x in labels]
        paths_indexes = [int(os.path.basename(path).split('-')[0]) for path in paths]
        
        label_set = set(labels_indexes)
        path_set = set(paths_indexes)

        for index in label_set ^ path_set:
            labels.append([index, 'not_card', ''])

    with open(labels_filename, 'w') as filehandle:
        json.dump(labels, filehandle, indent=2)

def flatten(input_folder, output_folder):
    from shutil import copyfile

    try:    
        os.mkdir(output_folder)
    except FileExistsError:
        print("Folder: '{}' already exists".format(output_folder))

    for i in range(52):
        paths = glob("{}\\{}\\*.png".format(input_folder, i))
        for path in paths:            
            output_path = "{}\\{}".format(output_folder, os.path.basename(path))          
            copyfile(path, output_path)
            
                
if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == "label":
            create_labels("training_data_split", "generated_labels.txt",
                        num_corners=2, check_completeness=False)
        elif sys.argv[1] == "split":
            split()
        elif sys.argv[1] == "flatten":
            flatten("training_data_split", "training_data")